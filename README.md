# Compassion Sponsored Child Plugin
---

A Wordpress plugin that displays the number of children sponsored through the Compassion program for the day as well as an image of the most recently sponsored child.

## Installation
---

You can use the following steps to install the Compassion Sponsored Child plugin:
1. Download the .ZIP file.
2. Extract the ZIP file.
3. Upload the extracted ``compassion-sponsored-child`` folder to the ``/wp-content/plugins/`` directory.
4. Activate the plugin through the Plugins menu in the Wordpress Admin.

## Usage
---

The Compassion Sponsored Child plugin provides a widget and shortcode for usage on your Wordpress site. To use the widget, you navigation to the Appearance->Widgets section of the Wordpress Admin. Then drag the widget to the area you wish to see it displayed in.

To use the plugin as a shortcode, you can embed the ``[compassion_sponsored]`` shortcode in to any post or page.

## Notes
---

If you include multiple instances of the shortcode or plugin on the same page, only one of them will display.

Browsers that have an ad-blocker installed and enabled may prevent the content from displaying.